use travel;

/* Females and male passengers travelle for a min distance of 600kms */
SELECT COUNT(CASE WHEN(Gender)='F' THEN 1 END) Female FROM passenger WHERE Distance>=600;

 /* Find the minimum ticket price for Sleeper Bus.  */
SELECT MIN(Price) FROM Price WHERE Bus_Type='Sleeper';

/* Select passenger names whose names start with character 'S' */
SELECT * FROM Passenger WHERE Passenger_name LIKE 's%';

/* calculate price charged for each passenger */
 SELECT Passenger_name,p.Boarding_City,p.Destination_City,p.Bus_type,pr.Price FROM passenger p,price pr where p.Distance=pr.Distance and p.Bus_Type=pr.Bus_Type;
 
 /* What is the passenger name and his/her ticket price who travelled in Sitting bus  for a distance of 1000 KMs. */
SELECT p.Passenger_name,p.Boarding_City,p.Destination_City,p.Bus_type,pr.Price FROM passenger p,price pr WHERE p.Distance=1000 and p.Bus_Type='Sitting' and p.Distance=1000 and pr.Bus_Type='Sitting';

/* What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji? */
SELECT p.Passenger_name,p.Boarding_City as Destination_City ,p.Destination_City as Boarding_City ,p.Bus_type,pr.Price FROM passenger p,price pr WHERE Passenger_name='Pallavi' and p.Distance=pr.Distance;

/* List the distances from the "Passenger" table which are unique in descending order. */
SELECT DISTINCT distance FROM Passenger ORDER BY Distance desc;

/* display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers */
SELECT Passenger_name,Distance*100/(SELECT SUM(Distance) FROM Passenger)FROM Passenger GROUP BY Distance;

/* Create a view to see all passengers who travelled in AC Bus. */
CREATE VIEW passengers_view AS SELECT * FROM Passenger WHERE Category='AC';
SELECT *FROM passenger_view;

/* create a stored procedure to find total passengers travelled using sleeper buses */
 DELIMITER //
  CREATE procedure getCountPassenger()
    begin
    select count(*) from price where Bus_Type='Sleeper';
    end//
    call getCountPassenger();
    //
    
    /* Display 5 records at one time */
select * from passenger LIMIT 5;

 




